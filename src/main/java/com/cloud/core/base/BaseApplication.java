package com.cloud.core.base;

import com.cloud.core.filter.HuiXianRequestFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

/**
 * Description: SpringBoot 基础启动类
 *
 * @author : LuShunNeng
 * @date : Created in 2018/9/23 下午6:51
 */
public class BaseApplication {

  @Bean
  public FilterRegistrationBean testFilterRegistration() {
    FilterRegistrationBean registration = new FilterRegistrationBean();
    registration.setFilter(new HuiXianRequestFilter());
    registration.addUrlPatterns("/*");
    registration.setName("huiXianRequestFilter");
    registration.setOrder(-100);
    return registration;
  }
}
