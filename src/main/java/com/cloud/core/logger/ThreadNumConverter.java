package com.cloud.core.logger;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * Created by snlu on 2018/8/20.
 * 线程号转换类
 */
public class ThreadNumConverter extends ClassicConverter {

  /**
   * 当需要显示线程ID的时候，返回当前调用线程的ID
   */
  @Override
  public String convert(ILoggingEvent event) {
    return String.valueOf(Thread.currentThread().getId());
  }
}