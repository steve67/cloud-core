package com.cloud.core.filter;

import com.cloud.common.enums.RequestContextEnum;
import com.cloud.common.vo.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;

/**
 * Description:   .
 *
 * @author : LuShunNeng
 * @date : Created in 2018/12/19 下午4:29
 */
@Slf4j
public class HuiXianRequestFilter implements Filter {

    /**
     * 统一token前缀
     */
    private static final String TOKEN_PREFIX = "Bearer ";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request= (HttpServletRequest) servletRequest;

        RequestContext requestContext = RequestContext.getContext();
        String traceId = null;
        String value;
        for (RequestContextEnum requestContextEnum : RequestContextEnum.values()) {
            value = request.getHeader(requestContextEnum.getOutCode());

            if (requestContextEnum.equals(RequestContextEnum.TRACE_ID)){
                traceId = request.getHeader(RequestContextEnum.TRACE_ID.getOutCode());
                if (StringUtils.isBlank(traceId)) {
                    traceId = UUID.randomUUID().toString();
                }
                value = traceId;
            }else if (requestContextEnum.equals(RequestContextEnum.HX_TOKEN)){
                if (StringUtils.isNotBlank(value)) {
                    value = value.replace(TOKEN_PREFIX, "");
                }
            }

            if (StringUtils.isNotEmpty(value)){
                requestContext.addContextDetail(requestContextEnum.name(), value);
            }
        }
        // traceId
        MDC.put(RequestContextEnum.TRACE_ID.name(), traceId);
        log.info("请求url{}:",request.getRequestURI());

        chain.doFilter(servletRequest,response);

        log.debug("整个请求处理完毕清除 logback MDC");
        MDC.clear();
        RequestContext.clear();
    }

    @Override
    public void destroy() {

    }
}
