package com.cloud.core;

import com.cloud.core.aop.ControllerExceptionHandler;
import com.cloud.core.aop.ControllerInterceptor;
import com.cloud.core.config.HuiXianConfigurer;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * create at  2018/09/11 18:28
 *
 * @author yanggang
 */
@Configuration
@Import({
        ControllerExceptionHandler.class
        , ControllerInterceptor.class
        , HuiXianConfigurer.class
})
public class AutoConfiguration {
}
