package com.cloud.core.aop;

import com.cloud.common.exception.BusinessException;
import com.cloud.common.model.BizException;
import com.cloud.common.model.CommonError;
import com.cloud.common.model.SystemException;
import com.cloud.common.util.ApiResponseUtil;
import com.cloud.common.vo.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * create at  2018/09/05 10:47
 *
 * @author yanggang
 */
@RestControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

    /**
     * 自定义参数校验异常处理
     *
     * @param exception .
     *
     * @return .
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ApiResponse methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException exception) {
        log.info("自定义参数异常[WARN]: {}", exception.getMessage());
        log.warn("自定义参数异常: ", exception);
        return   ApiResponseUtil.errorResponse(CommonError.PARAM_ERROR,
                Objects.requireNonNull(exception.getBindingResult().getFieldError()).getDefaultMessage());
    }

    /**
     * 参数校验异常
     *
     * @param exception 绑定校验异常
     *
     * @return .
     */
    @ExceptionHandler({BindException.class})
    public ApiResponse bindExceptionHandler(BindException exception) {
        log.info("参数异常[WARN]: {}", exception.getMessage());
        log.warn("参数异常: ", exception);
        return   ApiResponseUtil.errorResponse(CommonError.PARAM_ERROR,
                Objects.requireNonNull(exception.getBindingResult().getFieldError()).getDefaultMessage());
    }


    @ExceptionHandler({ConstraintViolationException.class})
    public ApiResponse constraintViolationExceptionHandler(ConstraintViolationException exception) {
        log.info("自定义参数异常[WARN]: {}", exception.getMessage());
        log.warn("自定义参数异常: ", exception);
        final List<String> errorList = new ArrayList<>(exception.getConstraintViolations().size());
        exception.getConstraintViolations().forEach(item -> {
            errorList.add(item.getMessageTemplate());
        });
        return ApiResponseUtil.errorResponse(CommonError.PARAM_ERROR, StringUtils.join(errorList,"|"));
    }

    /**
     * SystemException异常
     *
     * @param exception .
     *
     * @return .
     */
    @Deprecated
    @ExceptionHandler({SystemException.class})
    public ApiResponse systemExceptionHandler(SystemException exception) {
        log.info("SystemException异常[ERROR]: {}", exception.getMessage());
        log.error("SystemException异常: ", exception);
        ApiResponse apiResponse;
        if (exception.getErrorCode() != null) {
            apiResponse = ApiResponseUtil.errorResponse(exception.getErrorCode(),exception.getMessage());
        }else {
            apiResponse = ApiResponseUtil.errorResponse(exception.getError(),exception.getMessage());
        }
        return apiResponse;
    }

    /**
     * BizException异常
     *
     * @param exception .
     *
     * @return .
     */
    @Deprecated
    @ExceptionHandler({BizException.class})
    public ApiResponse bizExceptionHandler(BizException exception) {
        log.info("BizException异常[WARN]: {}", exception.getMessage());
        log.warn("BizException异常: ", exception);
        ApiResponse apiResponse;
        if (exception.getErrorCode() != null) {
            apiResponse =  ApiResponseUtil.errorResponse(exception.getErrorCode(),exception.getMessage());
        }else {
            apiResponse = ApiResponseUtil.errorResponse(exception.getError(),exception.getMessage());
        }
        return apiResponse;
    }


    /**
     * huixianException 异常
     *
     * @param huixianException 慧贤异常
     *
     * @return Api Json格式
     */
    @ExceptionHandler({BusinessException.class})
    public ApiResponse exception(BusinessException huixianException) {
        log.info(huixianException.getClass().getName() + " 异常[WARN]: {}", huixianException.getMessage());
        log.warn(huixianException.getClass().getName() + " 异常: ", huixianException);

        return ApiResponseUtil.errorResponse(huixianException);
    }

    /**
     * 全局异常
     *
     * @param exception .
     *
     * @return .
     */
    @ExceptionHandler({Exception.class})
    public ApiResponse exceptionHandler(Exception exception) {
        log.info("全局异常[ERROR]: {}", exception.getMessage());
        log.error("全局异常: ", exception);
        return ApiResponseUtil.errorResponse();
    }

}
