package com.cloud.core.aop;

import com.cloud.common.enums.RequestContextEnum;
import com.cloud.common.vo.ApiResponse;
import com.cloud.common.vo.RequestContext;
import com.cloud.core.annotation.IgnoreReqLog;
import com.cloud.core.annotation.IgnoreReqRespLog;
import com.cloud.core.annotation.IgnoreRespLog;
import com.cloud.core.util.HttpServletRequestUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * create at  2018/09/13 14:43
 *
 * @author yanggang
 */
@Aspect
@Order(1)
@Slf4j
public class ControllerInterceptor {

    @Autowired
    private HttpServletRequest request;

    @Pointcut("execution (* com.cloud.*.controller..*Controller.*(..))")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object interceptor(ProceedingJoinPoint pjp) throws Throwable {
        MethodSignature methodSignature = ((MethodSignature) pjp.getSignature());
        boolean ignoreReq = false;
        boolean ignoreResp = false;
        Method method = pjp.getTarget().getClass()
                .getMethod(pjp.getSignature().getName(), methodSignature.getParameterTypes());
        if (method.isAnnotationPresent(IgnoreReqLog.class)) {
            ignoreReq = true;
        }
        if (method.isAnnotationPresent(IgnoreRespLog.class)) {
            ignoreResp = true;
        }
        if (method.isAnnotationPresent(IgnoreReqRespLog.class)) {
            ignoreReq = true;
            ignoreResp = true;
        }

        beforeInterceptorLog(pjp, ignoreReq);

        Object result = pjp.proceed();
        String token = RequestContext.getContext()
                .getContextDetail(RequestContextEnum.HX_RETURN_TOKEN.name());
        if (result != null
                && result instanceof ApiResponse
                && StringUtils.isNotBlank(token)) {
            ApiResponse apiResponse = (ApiResponse) result;
            apiResponse.setToken(token);
            result = apiResponse;
        }

        afterInterceptorLog(pjp, result, ignoreResp);
        return result;
    }


    /**
     * 拦截器前打LOG的工具类
     *
     * @param pjp
     */
    private void beforeInterceptorLog(ProceedingJoinPoint pjp, boolean ignoreReq) {
        try {
            String className = pjp.getTarget().getClass().getSimpleName();
            String methodName = pjp.getSignature().getName();
            Signature signature = pjp.getSignature();
            MethodSignature methodSignature = (MethodSignature) signature;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("uri[").append(request.getRequestURI()).append("], ");
            stringBuilder.append("ip[").append(HttpServletRequestUtil.getRequestIp(request)).append("], ");
            stringBuilder.append("method[").append(className).append("@").append(methodName)
                    .append("] ");

            for (int i = 0; i < pjp.getArgs().length; i++) {
                stringBuilder.append("参数名:");
                stringBuilder.append(methodSignature.getParameterNames()[i]);
                stringBuilder.append(",参数值:");
                stringBuilder.append(pjp.getArgs()[i]);
            }
            if (ignoreReq) {
                log.debug(stringBuilder.toString());
            } else {
                log.info(stringBuilder.toString());
            }
        } catch (Exception e) {
            log.error("打印日志异常", e);
        }
    }

    /**
     * 拦截器后打LOG的工具类
     *
     * @param pjp
     */
    private void afterInterceptorLog(ProceedingJoinPoint pjp, Object result, boolean ignoreResp) {
        try {
            String className = pjp.getTarget().getClass().getSimpleName();
            String methodName = pjp.getSignature().getName();
            if (ignoreResp) {
                log.debug(StringUtils
                        .join("method[", className, "@", methodName, "], 结果为: ", result));
            } else {
                log.info(StringUtils
                        .join("method[", className, "@", methodName, "], 结果为: ", result));
            }
        } catch (Exception e) {
            log.error("打印日志异常", e);
        }
    }

}
