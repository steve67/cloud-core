package com.cloud.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Created by snlu on 2018/9/22.
 * 慧贤系统配置类
 */

@Validated
@ConfigurationProperties(
        prefix = "com.cloud"
)
@Component
public class HuiXianConfigurer implements Serializable {
    private static final long serialVersionUID = -8083824196026972702L;
    @NotBlank
    private String systemNum;

    public HuiXianConfigurer() {
    }

    public String getSystemNum() {
        return this.systemNum;
    }

    public void setSystemNum(String systemNum) {
        this.systemNum = systemNum;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof HuiXianConfigurer)) {
            return false;
        } else {
            HuiXianConfigurer other = (HuiXianConfigurer) o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$systemNum = this.getSystemNum();
                Object other$systemNum = other.getSystemNum();
                if (this$systemNum == null) {
                    if (other$systemNum != null) {
                        return false;
                    }
                } else if (!this$systemNum.equals(other$systemNum)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof HuiXianConfigurer;
    }

    @Override
    public int hashCode() {
        boolean PRIME = true;
        int result = 1;
        Object $systemNum = this.getSystemNum();
        result = result * 59 + ($systemNum == null ? 43 : $systemNum.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "HuiXianConfigurer(systemNum=" + this.getSystemNum() + ")";
    }
}
