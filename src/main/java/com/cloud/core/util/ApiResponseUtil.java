package com.cloud.core.util;

import com.cloud.common.enums.RequestContextEnum;
import com.cloud.common.exception.BusinessException;
import com.cloud.common.exception.FrameworkException;
import com.cloud.common.util.PropertiesUtil;
import com.cloud.common.vo.ApiResponse;
import com.cloud.common.vo.RequestContext;
import org.apache.commons.lang3.StringUtils;

/**
 * ApiResponse 处理工具类
 *
 * @author : polegek
 * @date : Created in 2019-10-21 16:34
 */
@Deprecated
public class ApiResponseUtil {

    /**
     * 获取错误码
     * 框架异常若没有组装，且不是特殊错误码，则封装为系统错误
     *
     * @param frameworkException 框架错误异常
     *
     * @return 错误码
     */
    public static Integer getErrorCode(FrameworkException frameworkException) {
        int errorCode = 0;

        // 如果异常不存在，在初始化化默认异常
        if (frameworkException == null) {
            frameworkException = new FrameworkException();
        }

        // 错误码不为0，则已拼接完成，直接返回
        if (frameworkException.getErrorCode() != 0) {
            return frameworkException.getErrorCode();
        }

        return getErrorCode(new BusinessException());
    }

    /**
     * 获取错误码
     *
     * @param huixianException 错误异常
     *
     * @return 错误码
     */
    public static Integer getErrorCode(BusinessException huixianException) {
        int errorCode = 0;

        // 如果异常不存在，在初始化化默认异常
        if (huixianException == null) {
            huixianException = new BusinessException();
        }

        // 错误码不为0，则已拼接完成，直接返回
        if (huixianException.getErrorCode() != 0) {
            return huixianException.getErrorCode();
        }

        // 拼接错误码
        int systemNum = Integer.parseInt(PropertiesUtil.getProperty("com.cloud.system-num"));
        String baseCode = StringUtils.leftPad(String.valueOf(huixianException.getBaseCode()), 3, "0");
        errorCode = Integer
                .parseInt(StringUtils
                        .join(systemNum, huixianException.getLayer(),
                                baseCode));
        return errorCode;
    }

    /**
     * 构建错误响应
     *
     * @param huixianException HuixianException
     *
     * @return 错误响应
     */
    public static ApiResponse errorResponse(BusinessException huixianException) {
        return errorResponse(getErrorCode(huixianException), huixianException.getMessage());
    }

    /**
     * 构建错误响应
     *
     * @return 错误响应
     */
    public static ApiResponse errorResponse() {
        return errorResponse(new BusinessException());
    }

    public static ApiResponse errorResponse(Integer errorCode, String errorStr) {
        String token = RequestContext.getContext().getContextDetail(RequestContextEnum.HX_RETURN_TOKEN.name());
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setErrorCode(errorCode);
        apiResponse.setErrorStr(errorStr);
        apiResponse.setToken(token);
        return apiResponse;
    }
}
